# ChessBoard Makefile
# @Author = natisdale@mail.csuchico.edu
#@Date = September 12, 2018

TARGET       = fix
OBJS         = fix.o
REBUILDABLES = $(TARGET) $(OBJS)
CC           = g++
FLAGS        = -g -Wall -pedantic -std=c++11
OTHERFLAGS   = -lstdc++
INCLIB       =

# Driver rules ...

$(TARGET): $(OBJS)
	$(CC) $(FLAGS) $(OTHERFLAGS) -o $@ $^ $(INCLIB)

# Standard rules ...

%.o: %.cpp
	$(CC) $(FLAGS) -o $@ -c $<

# Additional make options ...

test:
	@./fix
	@echo "Test Completed ..."

clean:
	@rm -f $(REBUILDABLES)
	@echo "All rebuildables deleted!"
