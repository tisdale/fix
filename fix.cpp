/*
 * File: fix.cpp
 * Author: Nathan Tisdale
 * Brief: Exploring prefix, infix, postfix conversions
 */
#include <iostream>
#include <stack>
using namespace std;

void inFixToPostFix(string inFix){
        stack<string> operands;
        for (string c : inFix){
                if ((c == '+') || (c == '-') || (c == '/') || (c == '*')){
                        operands.push(c);
                } else {
                        cout << c;
                }
        }
        for (char c : operands){
                cout << operands.top();
        }
}

int main(){
        string infix = "a*b/c";
        string postfix = "abc/*";
        string prefix = "*ab\c";

        inFixToPostFix(infix);
        return 0;
}
